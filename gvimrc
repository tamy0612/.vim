set showtabline=2
set imdisable
set antialias
set guifont=Ricty\ 12
set guioptions&
set guioptions-=T
set guicursor=a:blinkon0
set lines=52 columns=120
if g:is_windows
  set transparency=10
endif
