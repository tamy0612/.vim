# Vim directory

## Version
for over Vim-7.3

## Installation

```sh
git clone git://github.com/tamy0612/.vim .vim
sh .vim/scripts/install.sh
```

`install.sh`

- install [neobundle.vim](https://github.com/Shougo/neobundle.vim)
- install [vimproc](https://github.com/Shougo/vimproc)
