let g:dict = {
      \ 'Pry' : 'pry',
      \ 'Gosh' : 'gosh',
      \ 'Scala' : 'scala',
      \ 'Node' : 'node',
      \}

call commands#repl#define( g:dict )
