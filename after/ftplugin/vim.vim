" call help command for a word under the cursor
nnoremap <buffer> <silent> <Leader>h :<C-u>help<Space><C-r><C-w><CR>
