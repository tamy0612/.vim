"================================================
" tamy's .vimrc
"================================================
" finish loading if vim <= small
if !1 | finish | end

"================================================
" Initialize
"================================================
let g:is_windows  = has('win16') || has('win32') || has('win64')
let g:is_cygwin   = has('win32unix')
let g:is_osx      = !g:is_windows && !g:is_cygwin
      \ && ( has('mac') || has('macunix') || has('gui_macvim')
      \       || ( !executable('xdg-open') && system('uname') =~? '^darwin' ) )
let g:is_linux    = !g:is_windows && !g:is_cygwin && !g:is_osx && has('unix')

let s:is_gui = has('gui_running')

if g:is_windows
  language message en
  set shellslash
else
  language message C
endif

"================================================
" Let global variables
"================================================
let $VIMDIR   = $HOME . '/.vim'
let $MYVIMRC  = $HOME . '/.vimrc'
if filereadable( $HOME . '/.vimrc.mine' )
  let $SECRETVIMRC = $HOME . '/.vimrc.mine'
  source $SECRETVIMRC
  let s:secret_vimrc_loaded = 1
endif
if s:is_gui
  let $MYGVIMRC = $HOME . '/.gvimrc'
endif

"================================================
" Set runtimepath for windows
"================================================
if g:is_windows
  set runtimepath^=$VIMDIR
  set runtimepath+=$VIMDIR/after
endif

"================================================
" neobundle
"================================================
filetype plugin indent off
if has('vim_starting')
  set runtimepath+=$VIMDIR/bundle/neobundle.vim
endif
let g:neobundle#enable_tail_path = 1
let g:neobundle#default_options = { 'default' : { 'overwrite' : 0 } }
call neobundle#rc( expand( $VIMDIR . '/bundle/' ) )

" NeoBundle core
NeoBundleFetch 'Shougo/neobundle.vim'
NeoBundle 'Shougo/neobundle-vim-scripts'

" vimproc
NeoBundle 'Shougo/vimproc'
call neobundle#config( 'vimproc', {
      \ 'build' : {
      \   'mac' : 'make -f make_mac.mak',
      \   'unix' : 'make -f make_unix.mak',
      \ }})

" complement
NeoBundleLazy 'Shougo/neocomplete.vim', {
      \ 'autoload' : {
      \   'insert' : 1,
      \ }}
NeoBundleLazy 'Shougo/neosnippet', {
      \ 'autoload' : {
      \   'insert' : 1,
      \   'filetypes' : ['snippet'],
      \   'unite_sources' : ['snippet', 'neosnippet/user', 'neosnippet/runtime'],
      \ }}

" unite
NeoBundleLazy 'Shougo/unite.vim', {
      \ 'autoload' : {
      \   'commands' : [{ 'name' : 'Unite',
      \                   'complete' : 'customlist,unite#complete_source'},
      \                 'UniteWithCursorWord', 'UniteWithInput', 'UniteBookmarkAdd']
      \ }}
NeoBundleLazy 'Shougo/unite-build', {
      \ 'depends' : 'Shougo/unite.vim',
      \ 'autoload' : {
      \   'unite_sources' : 'build'
      \ }}
NeoBundleLazy 'Shougo/unite-outline', {
      \ 'depends' : 'Shougo/unite.vim',
      \ 'autoload' : {
      \   'unite_sources' : 'outline'
      \ }}
NeoBundleLazy 'Shougo/unite-ssh', {
      \ 'depends' : 'Shougo/unite.vim',
      \ 'autoload' : {
      \   'unite_sources' : 'ssh'
      \ }}
NeoBundleLazy 'osyo-manga/unite-quickfix', {
      \ 'depends' : 'Shougo/unite.vim',
      \ 'autoload' : {
      \   'unite_sources' : 'quickfix'
      \ }}
NeoBundleLazy 'thinca/vim-unite-history', {
      \ 'depends' : 'Shougo/unite.vim',
      \ 'autoload' : {
      \   'unite_sources' : ['history/command', 'history/search']
      \ }}
NeoBundleLazy 'pasela/unite-webcolorname', {
      \ 'depends' : 'Shougo/unite.vim',
      \ 'autoload' : {
      \   'unite_sources' : ['webcolorname'],
      \ }}
NeoBundleLazy 'ujihisa/unite-colorscheme', {
      \ 'depends' : 'Shougo/unite.vim',
      \ 'autoload' : {
      \   'unite_sources' : ['colorscheme'],
      \ }}

" filer
NeoBundleLazy 'Shougo/vimfiler', {
      \ 'depends' : 'Shougo/unite.vim',
      \ 'autoload' : {
      \   'commands' : [
      \                  { 'name' : 'VimFiler',
      \                    'complete' : 'customlist,vimfiler#complete' },
      \                  { 'name' : 'VimFilerExplorer',
      \                    'complete' : 'customlist,vimfiler#complete' },
      \                  { 'name' : 'Edit',
      \                    'complete' : 'customlist,vimfiler#complete' },
      \                  { 'name' : 'Write',
      \                    'complete' : 'customlist,vimfiler#complete' },
      \                  'Read', 'Source'],
      \   'mappings' : ['<Plug>(vimfiler_switch)'],
      \   'explorer' : 1,
      \ }}
NeoBundleLazy 'kien/ctrlp.vim', {
      \ 'autoload' : {
      \   'commands' : ['CtrlP'],
      \   'mappings' : ['n','<C-p>'],
      \ }}
NeoBundleLazy 'tacahiroy/ctrlp-funky', {
      \ 'depends' : 'kien/ctrlp.vim',
      \ 'autoload' : {
      \   'commands' : ['CtrlPFunky'],
      \}}

" vimshell
NeoBundleLazy 'Shougo/vimshell', {
      \ 'autoload' : {
      \   'commands' : [{ 'name' : 'VimShell',
      \                   'complete' : 'customlist,vimshell#complete'},
      \                 'VimShellExecute', 'VimShellInteractive',
      \                 'VimShellterminal', 'VimShellPop'],
      \   'mappings' : ['<Plug>(vimshell_switch)']
      \ }}
NeoBundleLazy 'ujihisa/vimshell-ssh', {
      \ 'depends' : 'Shougo/vimshell',
      \ 'autoload' : {
      \   'filetypes' : ['vimshell'],
      \ }}
NeoBundleLazy 'yomi322/vim-gitcomplete', {
      \ 'autoload' : {
      \   'filetypes' : ['vimshell']
      \ }}

" util
NeoBundleLazy 'mbbill/undotree', {
      \ 'autoload' : {
      \   'commands' : ['UndotreeToggle']
      \ }}
NeoBundleLazy 'thinca/vim-quickrun', {
      \ 'autoload' : {
      \   'mappings' : ['<Plug>(quickrun)']
      \ }}
NeoBundleLazy 'itchyny/calendar.vim', {
      \ 'autoload' : {
      \   'commands' : ['Calendar']
      \ }}
NeoBundleLazy 'Yggdroot/indentLine', {
      \ 'autoload' : {
      \   'commands' : ['IndentLineEnable','IndentLineToggle'],
      \}}
NeoBundle 'tpope/vim-commentary'

" operator
NeoBundleLazy 'kana/vim-operator-user', {
      \   'functions' : 'operator#user#define',
      \ }
NeoBundleLazy 'kana/vim-operator-replace', {
      \ 'depends' : 'vim-operator-user',
      \ 'autoload' : {
      \   'mappings' : [
      \     ['nx', '<Plug>(operator-replace)']]
      \ }}
NeoBundleLazy 'rhysd/vim-operator-surround', {
      \ 'depends' : 'vim-operator-user',
      \   'mappings' : '<Plug>(operator-surround',
      \ }

" visual
NeoBundle 'itchyny/lightline.vim'
NeoBundle 'CSApprox'

" plugins depend on each environment
NeoBundleLazy 'kana/vim-smartinput', {
      \ 'autoload' : {
      \   'filetypes' : [
      \     'c', 'cpp', 'd', 'go', 'java', 'scala', 'css', 'sass', 'php'
      \   ]
      \ }}
NeoBundleLazy 'taichouchou2/vim-endwise.git', {
      \ 'autoload' : {
      \   'filetypes' : [
      \     'ruby', 'lua', 'sh', 'zsh', 'vb', 'vbnet', 'aspvbs', 'vim',
      \   ],
      \ }}
NeoBundleLazy 'ujihisa/unite-rake', {
      \ 'depends' : 'Shougo/unite.vim',
      \ 'autoload' : {
      \   'filetypes' : ['ruby','d'],
      \ }}
NeoBundleLazy 'Shougo/neocomplcache-rsense', {
      \ 'depends' : ['Shougo/neocomplcache','Shougo/vimproc'],
      \ 'autoload' : { 'filetypes' : ['ruby'] }
      \ }
NeoBundleLazy 'rhysd/neco-ruby-keyword-args', {
      \ 'depends' : 'Shougo/neocomplcache',
      \ 'autoload' : {
      \   'filetypes' : ['ruby'],
      \ }}
NeoBundleLazy 'rhysd/unite-ruby-require.vim', {
      \ 'depends' : 'Shougo/unite.vim',
      \ 'autoload' : {
      \   'filetypes' : ['ruby'],
      \ }}
NeoBundleLazy 'tamy0612/d.vim', {
      \ 'autoload' : {
      \   'filetypes' : ['d','di'],
      \ }}
NeoBundleLazy 'tamy0612/erlang.vim', {
      \ 'autoload' : {
      \   'filetype' : 'erlang',
      \ }}
NeoBundleLazy 'hail2u/vim-css3-syntax', {
      \ 'autoload' : {
      \   'filetypes' : ['css'],
      \ }}
NeoBundleLazy 'taichouchou2/html5.vim', {
      \ 'autoload' : {
      \   'filetypes' : [
      \     'html',
      \     'php',
      \     'eruby',
      \   ],
      \ }}
NeoBundle 'tamy0612/template.vim'

if filereadable( $HOME . '/.neobundle.local' )
  let $LOCALBUNDLES = $HOME . '/.neobundle.local'
  source $LOCALBUNDLES
endif

NeoBundleLocal $VIMDIR.'/bundle'

filetype plugin indent on
syntax enable

NeoBundleCheck
let g:neobundle_loaded = 1

"================================================
" environment
"================================================
" Set backupdir $HOME/.vim/back
set backupdir=$VIMDIR/back
" Don't make the swapfile
set noswapfile
"let &directory = &backupdir
" Make undofile
set undofile
let &undodir = &backupdir
" use login shell
set shell=zsh\ -l

"================================================
" encode
"================================================
set enc=utf-8
set fenc=utf-8
set fencs=iso-2022-jp,utf-8,euc-jp,cp932
set formatoptions&
set formatoptions+=mM
set formatexpr=autofmt#japanese#formatexpr()
set fileformat=unix
set fileformats=unix,mac,dos

"================================================
" view
"================================================
" Set title the file path
let &titleold = ""
let &titlestring = "%f"
" Show row number
set number
augroup numberwidth
  autocmd!
  autocmd BufEnter,WinEnter,BufWinEnter * let &l:numberwidth = len(line("$")) + 2
augroup END
" Don't show escape chars
set nolist
" Show status line
set laststatus=2
" cmd height = 1
set cmdheight=1
" Show commandsd
set showcmd
" colors
set t_Co=256
set background=dark
colorscheme moonshade
syntax on
" Show matches
set showmatch

"================================================
" edit
"================================================
" Indent
set autoindent
set smartindent
set cindent
set cinoptions&
set cinoptions+=:0,g4,N0,h0,f0
" Tab
set tabstop=2
set shiftwidth=2
set smarttab
set expandtab
" Folding
set foldmethod=syntax
set foldlevel=100 " Deny folding
" wrapping
set whichwrap=b,s,h,l,<,>,[,]
set backspace=indent,eol,start
set wildmenu
" Hiden buffer
set hidden
" Deny auto CR
set textwidth=0
" window
set splitright
set splitbelow
set scrolloff=10
set lazyredraw
set modeline

"================================================
" search
"================================================
set hlsearch
set nowrapscan
set ignorecase
set smartcase
set incsearch
set wildignore=*.o,*.class,*.beam,*.dvi,*.pdf

"================================================
" plugin configurations
"================================================
augroup AutoCmd "{{{
  autocmd!
  " mkdir when the target directory does not exist
  autocmd BufWritePre * call hook#write#auto_mkdir( expand('<afile>:p:h'), v:cmdbang )
  " remove extra spaces
  autocmd BufWritePre * call hook#write#space_clean()
  " reload ~/.vimrc when it was updated
  autocmd BufWritePost $MYVIMRC source $MYVIMRC
  " reload ~/.vimrc.mine when it was updated  => only when .vimrc.mine exists
  if exists('s:secret_vimrc_loaded')
    autocmd BufWritePost $SECRETVIMRC source $SECRETVIMRC
  endif
  " reload ~/.gvimrc when it was updated  => only used in gVim
  if s:is_gui
    autocmd BufWritePost $MYGVIMRC source $MYGVIMRC
  endif
  " autocmd for each filetype
  autocmd FileType gitcommit execute "normal! gg"
  " omnifunc
  "autocmd FileType ada setlocal omnifunc=adacomplete#Complete
  autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
  autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
  autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
  autocmd FileType sql setlocal omnifunc=sqlcomplete#Complete
  autocmd FileType c setlocal omnifunc=ccomplete#Complete
  autocmd FileType ruby setlocal omnifunc=rubycomplete#Complete
  autocmd FileType java setlocal omnifunc=javacomplete#Complete
  autocmd FileType php setlocal omnifunc=phpcomplete#CompletePHP
  if has('python3')
    autocmd FileType python setlocal omnifunc=python3complete#Complete
  else
    autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
  endif
augroup END "}}}

" unite "{{{
if neobundle#tap('unite.vim')
  nnoremap [unite] <Nop>
  " mapping '<Leader>u' will be depricated
  nmap <Leader>u [unite]
  " [unite] uses '<Space>'
  nmap <Space> [unite]
  nmap <silent> [unite]f :<C-u>Unite -vertical -winwidth=30 file<CR>
  nmap <silent> [unite]b :<C-u>Unite -winheight=10 buffer<CR>
  nmap <silent> [unite]r :<C-u>Unite -winheight=10 -buffer-name=register register<CR>
  nmap <silent> [unite]o :<C-u>Unite -vertical -winwidth=30 -no-quit outline<CR>
  nmap <silent> [unite]t :<C-u>Unite -winheight=10 tab<CR>
  nmap <silent> [unite]w :<C-u>Unite -winheight=10 window<CR>
  nmap <silent> [unite]m :<C-u>Unite -winheight=15 mapping<CR>
  nmap <silent> [unite]s :<C-u>Unite -winheight=10 menu:shortcut<CR>
  nmap <silent> [unite]c :<C-u>Unite -vertical -winwidth=30 bookmark<CR>
  nmap <silent> [unite]a :<C-u>UniteBookmarkAdd<CR>
  function! s:deine_unite_keymappings()
    nnoremap <silent><buffer><expr> s unite#smart_map('s', unite#do_action('split'))
    inoremap <silent><buffer><expr> s unite#smart_map('s', unite#do_action('split'))
    nnoremap <silent><buffer><expr> v unite#smart_map('v', unite#do_action('vsplit'))
    inoremap <silent><buffer><expr> v unite#smart_map('v', unite#do_action('vsplit'))
    nnoremap <silent><buffer><expr> f unite#smart_map('f', unite#do_action('vimfiler'))
    inoremap <silent><buffer><expr> f unite#smart_map('f', unite#do_action('vimfiler'))
  endfunction
  function! neobundle#tapped.hooks.on_source( bundle )
    let g:unite_enable_start_insert = 0
    let g:unite_source_menu_menus = {
          \ 'shortcut' : {
          \   'description' : 'my shortcut',
          \   'command_candidates' : [
          \     [ 'Unite output:messages' , 'Unite output:messages' ],
          \   ],
          \ },}
    let g:unite_source_file_mru_limit = 50
    let g:unite_source_file_mru_filename_format = ''
  endfunction
  autocmd AutoCmd FileType unite call s:deine_unite_keymappings()
  call neobundle#untap()
endif
"}}}

" unite-outline "{{{
if neobundle#tap('unite-outline')
  function! neobundle#tapped.hooks.on_source( bundle )
    let s:ftype_alias = {
          \ 'tex' : ['latex'],
          \}
    for [ftype, aliases] in items( s:ftype_alias )
      call call('unite#sources#outline#alias', [aliases, ftype])
    endfor
  endfunction
  call neobundle#untap()
endif
"}}}

" neocomplete "{{{
if neobundle#tap('neocomplete.vim')
  function! neobundle#tapped.hooks.on_source( bundle )
    let g:acp_enableAtStartup = 0
    let g:neocomplete#enable_at_startup = 1
    let g:neocomplete#enable_smart_case = 1
    let g:neocomplete#enable_auto_select = 0
    let g:neocomplete#enable_fuzzy_completion = 1
    let g:neocomplete#max_list = 10
    let g:neocomplete#use_vimproc = 1
    let g:neocomplete#dictionary_filetype_lists = {
          \ 'php' : $VIMDIR.'/dict/php.dict',
          \ 'scala' : $VIMDIR.'/dict/scala.dict',
          \ 'vimshell' : $HOME.'/.vimshell/command-history' }
    if !exists('g:neocomplete#sources#omni#input_patterns')
      let g:neocomplete#sources#omni#input_patterns = {}
    endif
    let g:neocomplete#sources#omni#input_patterns.ruby = '[^. *\t]\.\w*\|\h\w*::\w*'
    let g:neocomplete#sources#vim#complete_functions = {
          \ 'Unite' : 'unite#complete_source',
          \ 'VimShellExecute' :
          \      'vimshell#vimshell_execute_complete',
          \ 'VimShellInteractive' :
          \      'vimshell#vimshell_execute_complete',
          \ 'VimShellTerminal' :
          \      'vimshell#vimshell_execute_complete',
          \ 'VimShell' : 'vimshell#complete',
          \ 'VimFiler' : 'vimfiler#complete' }
  endfunction
endif
"}}}

" neocomplcache "{{{
if neobundle#tap('neocomplcache')
  function! neobundle#tapped.hooks.on_source( bundle )
    let g:acp_enableAtStartup = 0
    let g:neocomplcache_enable_at_startup = 0
    let g:neocomplcache_smartcase = 1
    let g:neocomplcache_enable_auto_select = 0
    let g:neocomplcache_enable_fuzzy_completion = 1
    let g:neocomplcache_max_list = 10
  endfunction
  call neobundle#untap()
endif
"}}}

" neosnippet "{{{
if neobundle#tap('neosnippet')
  if has('conceal')
    set conceallevel=2 concealcursor=i
  endif
  function! neobundle#tapped.hooks.on_source( bundle )
    let g:neosnippet#enable_snipmate_compatibility = 1
    let g:neosnippet#snippets_directory = $VIMDIR.'/snippets'
    imap <C-k> <Plug>(neosnippet_expand_or_jump)
    smap <C-k> <Plug>(neosnippet_expand_or_jump)
    imap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<S-TAB>"
    imap <expr><Down> pumvisible() ? "\<C-n>" : "\<Down>"
    imap <expr><Up> pumvisible() ? "\<C-p>" : "\<Up>"
    imap <expr><TAB> neosnippet#jumpable() ? "\<Plug>(neosnippet_expand_or_jump)" : pumvisible() ? "\<C-n>" : "\<TAB>"
    smap <expr><TAB> neosnippet#jumpable() ? "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"
  endfunction
  call neobundle#untap()
endif
"}}}

" VimFiler "{{{
if neobundle#tap('vimfiler')
  nmap <Leader>vf :VimFilerExplorer<CR>
  function! neobundle#tapped.hooks.on_source( bundle )
    let g:vimfiler_as_default_explorer = 1
    let g:vimfiler_safe_mode_by_default = 0
  endfunction
  call neobundle#untap()
endif
"}}}

" CtrlP "{{{
if neobundle#tap('ctrlp.vim')
  nnoremap <C-p> :<C-u>CtrlP<CR>
  function! neobundle#tapped.hooks.on_source( bundle )
    let g:ctrlp_working_path_mode = 'ra'
    let g:ctrlp_open_new_file = 'r'
    let g:ctrlp_extensions = ['tag', 'quickfix', 'dir', 'line', 'mixed']
    let g:ctrlp_match_window = 'bottom,order:btt,min:5,max:15'
  endfunction
  call neobundle#untap()
endif
"}}}

" quickrun "{{{
if neobundle#tap('vim-quickrun')
  nmap <silent> <Leader>r <Plug>(quickrun)
  function! neobundle#tapped.hooks.on_source( bundle )
    let g:quickrun_config = {}
    " global setting
    let g:quickrun_config._ = {
          \ 'runner' : 'vimproc',
          \ 'hook/time/enable' : 1,
          \ 'outputter/buffer/split' : 'botright 15sp',
          \}
    " for D-lang
    let g:quickrun_config['d/run'] = {
          \ 'type' : 'd/run',
          \ 'command' : 'dmd',
          \ 'exec' : '%c -run %s',
          \}
    let g:quickrun_config['d/test'] = {
          \ 'type' : 'd/test',
          \ 'command' : 'dmd',
          \ 'exec' : '%c -main -unittest %s',
          \}
    " for rspec
    let g:quickrun_config['rspec/bundle'] = {
          \ 'type' : 'rspec/bundle',
          \ 'command' : 'rspec',
          \ 'exec' : 'bundle exec %c %s',
          \}
    let g:quickrun_config['rspec/normal'] = {
          \ 'type' : 'rspec/normal',
          \ 'command' : 'rspec',
          \ 'exec' : '%c %s',
          \}
  endfunction
  call neobundle#untap()
endif
"}}}

" VimShell "{{{
if neobundle#tap('vimshell')
  noremap <Plug>(vimshell_toggle) :VimShell -toggle<CR>
  nmap <C-x> <Plug>(vimshell_toggle)
  imap <C-x> <C-[><Plug>(vimshell_toggle)
  nmap <Leader>x :VimShellPop<CR>
  function! neobundle#tapped.hooks.on_source( bundle )
    let g:vimshell_split_command = ''
    let g:vimshell_enable_transient_user_prompt = 1
    let g:vimshell_force_overwrite_statusline = 1
    let g:vimshell_execute_file_list = {}
    call vimshell#set_execute_file('txt,vim,log', 'vim')
    let g:vimshell_execute_file_list['rb'] = 'ruby'
    let g:vimshell_execute_file_list['pl'] = 'perl'
    let g:vimshell_execute_file_list['py'] = 'python'
    let g:vimshell_execute_file_list['js'] = 'node'
    let g:vimshell_execute_file_list['php'] = 'php'
    let g:vimshell_execute_file_list['d'] = 'rdmd'
  endfunction
  call neobundle#untap()
endif
"}}}

" Calendar.vim "{{{
if neobundle#tap('calendar.vim')
  function! neobundle#tapped.hooks.on_source( bundle )
    let g:calendar_google_calender = 1
    let g:calender_google_task = 1
  endfunction
  call neobundle#untap()
endif
"}}}

" indentLine "{{{
if neobundle#tap('indentLine')
  nnoremap <C-i> :<C-u>:IndentLineToggle<CR>
endif
"}}}

" operator-replace "{{{
if neobundle#tap('vim-operator-replace')
  xmap p <Plug>(operator-replace)
  call neobundle#untap()
endif
"}}}

" operator-surround "{{{
if neobundle#tap('vim-operator-surround')
  nmap <silent>sa <Plug>(operator-surround-append)a
  nmap <silent>sd <Plug>(operator-surround-delete)a
  nmap <silent>sr <Plug>(operator-surround-replace)a
  call neobundle#untap()
endif
"}}}

" lightline "{{{
if neobundle#tap('lightline.vim')
  let g:lightline = {
        \ 'colorscheme' : 'default',
        \ 'component' : {
        \   'readonly' : '%{&readonly?" x":""}'
        \ }}
call neobundle#untap()
endif
"}}}

" unite-ruby-require "{{{
if neobundle#tap('unite-ruby-require.vim')
  function! neobundle#tapped.hooks.on_source( bundle )
    let g:unite_source_ruby_require_ruby_command = $RBENV_ROOT.'/shims/ruby'
  endfunction
  call neobundle#untap()
endif
"}}}

" d.vim "{{{
if neobundle#tap('d.vim')
  autocmd AutoCmd BufNewFile *.d call d#InsertModuleDeclaration()
  call neobundle#untap()
endif
"}}}

" erlang.vim "{{{
if neobundle#tap('erlang.vim')
  autocmd AutoCmd BufNewFile *.erl call erlang#InsertModuleDeclaration()
  call neobundle#untap()
endif

"================================================
" key mappings
"================================================
" cursor
imap OA <UP>
imap OB <Down>
imap OC <Right>
imap OD <Left>
nnoremap j gj
onoremap j gj
xnoremap j gj
nnoremap k gk
onoremap k gk
xnoremap k gk
nnoremap <Down> gj
nnoremap <Up> gk

" mode change
imap <C-@> <C-[>

" window
nnoremap <Tab> <C-w>w
nnoremap <C-S-h> <C-w>h
nnoremap <C-S-Left> <C-w><Left>
nnoremap <C-S-j> <C-w>j
nnoremap <C-S-Down> <C-w><Down>
nnoremap <C-S-k> <C-w>k
nnoremap <C-S-Up> <C-w><Up>
nnoremap <C-S-l> <C-w>l
nnoremap <C-S-Right> <C-w><Right>

" indent
xnoremap <TAB>  >
xnoremap <S-TAB>  <
nnoremap > >>
nnoremap < <<
xnoremap > >gv
xnoremap < <gv
nnoremap <silent> <Leader>= :<C-u>execute "normal!gg=G"<CR>

" edit
nnoremap <Leader>e :<C-u>e
nnoremap <Leader>en :<C-u>enew<CR>
nnoremap <Leader>es :<C-u>Scratch<CR>
nmap <Esc><Esc> :<C-u>nohlsearch<CR><Esc>

" vim:ft=vim:ts=2:sw=2
